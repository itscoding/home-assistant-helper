# HomeAssistant Helper

PHP Applikation mit Routen für kleinere Helfer

### Routen

### Strompreis

#### Env Variabeln
- `TARIF_HIGH`  hoch tarif
- `TARIF_LOW`  niedertarif

`/price`

Liefert den aktuellen Strompreis je nach Zeit 

- Niedertarif: von 20.00 bis 07.00 und Samstag/Sonntag
- Hochtarif: Restliche Zeit
```json
{
  "price": 0.1708
}
```

### Synology NAS

### Env Variabeln

(User benötigt Rechte für Shutdown des NAS)

```dotenv
NAS_HOST=192.168.0.6
NAS_USER=user
NAS_PORT=22
NAS_PASSWORD=password
```

`/nas-on`
Schaltet das NAS ein

```json
{
  "state": "turn on command sent",
  "result": [
    
  ]
}
```

`/nas-off`
```json
{
  "state": "turn off command sent",
  "result": null
}
```

`/nas-state`
```json
{
  "power": true,
  "statusCode": 0,
  "result": [
    "PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.",
    "64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.053 ms",
    "",
    "--- 127.0.0.1 ping statistics ---",
    "1 packets transmitted, 1 received, 0% packet loss, time 0ms",
    "rtt min/avg/max/mdev = 0.053/0.053/0.053/0.000 ms"
  ]
}
```

### Docker Compose

[Docker Compose file (bsp.)](/_docker/production/docker-compose.yml)

Beispiel files für den Nginx Container etc. (Das Dockerfile, welches für das Build verwendet wurde)

[Docker Compose](/_docker/production/)

### Docker Images


##### Das Image mit der neusten Version:

- registry.gitlab.com/itscoding/home-assistant-helper:latest

##### Spezifische Version:

- registry.gitlab.com/itscoding/home-assistant-helper:1.0.0-alpha


#### Images für (Raspberry Pi)  --platform linux/arm64

- registry.gitlab.com/itscoding/home-assistant-helper:1.0.0-alpha-arm64 (keine latest version)



