#!/bin/bash

# the www-data user (php user) needs a valid sshkey to the nas!
ssh -o "StrictHostKeyChecking=no" -o "ConnectTimeout=1"  $NAS_USER@$NAS_HOST -p$NAS_PORT "echo $NAS_PASSWORD | sudo -S /usr/syno/sbin/synoshutdown -s"
