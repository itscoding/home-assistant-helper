#!/bin/bash

PORT=$1

status=$(sshpass -p "$SW_PASSWORD" ssh -o "StrictHostKeyChecking=no" -o "ConnectTimeout=1"  $SW_USER@$SW_HOST "swctrl poe show id $PORT" | tail -n1 | awk '{print $2}')

if [ "$status" == "Auto" ]; then
    echo "true"
else
    echo "false"
fi

