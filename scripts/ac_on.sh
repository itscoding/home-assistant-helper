#!/bin/bash

PORT=$1
sshpass -p "$SW_PASSWORD" ssh -o "StrictHostKeyChecking=no" -o "ConnectTimeout=1"  $SW_USER@$SW_HOST "swctrl poe set auto id $PORT"
