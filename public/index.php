<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();
$debug = (bool)getenv('APP_DEBUG');

$app->get('/nas-on', function (Request $request, Response $response) {
    exec(__DIR__ . '/../scripts/nas_on.sh', $result);
    $response->getBody()->write(
        json_encode([
            'state' => 'turn on command sent',
            'result' => $result,
        ])
    );
    $response->withHeader('Content-Type', 'application/json');
    return $response;
});

$app->get('/nas-off', function (Request $request, Response $response) {
    $result = shell_exec(__DIR__ . '/../scripts/nas_shutdown.sh');
    $response->getBody()->write(
        json_encode([
            'state' => 'turn off command sent',
            'result' => $result,
        ])
    );
    $response->withHeader('Content-Type', 'application/json');
    return $response;
});

$app->get('/nas-state', function (Request $request, Response $response) {
    $nasHost = getenv('NAS_HOST');
    exec("ping $nasHost -c 1 -W 1 -t 1", $result, $statusCode);
    $response->getBody()->write(
        json_encode([
            'power' => !$statusCode,
            'statusCode' => $statusCode,
            'result' => $result,
        ])
    );
    $response->withHeader('Content-Type', 'application/json');
    return $response;
});

$app->get('/price', function (Request $request, Response $response) {
    $time = (int)date('H');
    $day = (int)date('w');
    $niederTarif = getenv('TARIF_LOW');
    $hochTarif = getenv('TARIF_HIGH');

    $price = $niederTarif;
    if ($day !== 6 && $day !== 7) {
        if ($time > 7 && $time < 20) {
            $price = $hochTarif;
        }
    }
    $response->getBody()->write(
        json_encode([
            'price' => (float)$price,
        ])
    );
    $response->withHeader('Content-Type', 'application/json');
    return $response;
});

$allowedPorts = explode(',', getenv('SW_ALLOWED_POE_PORTS'));
$allowedPorts = array_map(fn ($port) => (int)$port, $allowedPorts);

$app->get('/poe/{port}/on', function (
    Request $request,
    Response $response
) use ($allowedPorts) {
    $port = (int)$request->getAttribute('port');
    if (!in_array($port, $allowedPorts)) {
        $response->getBody()->write(
            json_encode([
                'error' => 'port is required (given port is not allowed)',
            ])
        );
        $response->withHeader('Content-Type', 'application/json');
        $response->withStatus(401);
        return $response;
    }

    shell_exec(sprintf(__DIR__ . '/../scripts/ac_on.sh %s', $port));
    $response->getBody()->write(
        json_encode([
            'state' => 'turn on command sent',
            'port' => $port,
            'result' => 1,
        ])
    );
    return $response;
});

$app->get('/poe/{port}/off', function (
    Request $request,
    Response $response
) use ($allowedPorts) {
    $port = (int)$request->getAttribute('port');
    if (!in_array($port, $allowedPorts)) {
        $response->getBody()->write(
            json_encode([
                'error' => 'port is required (given port is not allowed)',
            ])
        );
        $response->withHeader('Content-Type', 'application/json');
        $response->withStatus(401);
        return $response;
    }

    shell_exec(sprintf(__DIR__ . '/../scripts/ac_off.sh %s', $port));
    $response->getBody()->write(
        json_encode([
            'state' => 'turn off command sent',
            'port' => $port,
            'result' => 0,
        ])
    );
    return $response;
});

$app->get('/poe/{port}/status',function (
    Request $request,
    Response $response
) use ($allowedPorts) {
    $port = (int)$request->getAttribute('port');
    if (!in_array($port, $allowedPorts)) {
        $response->getBody()->write(
            json_encode([
                'error' => 'port is required (given port is not allowed)',
            ])
        );
        $response->withHeader('Content-Type', 'application/json');
        $response->withStatus(401);
        return $response;
    }

    $result = shell_exec(sprintf(__DIR__ . '/../scripts/ac_status.sh %s', $port));
    $response->getBody()->write(
        json_encode([
            'state' => 'show state command sent',
            'port' => $port,
            'result' => trim($result, "\n") === "true",
        ])
    );
    return $response;
});


$errorMiddleware = $app->addErrorMiddleware($debug, $debug, $debug);

$app->run();
