#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0
set -xe

# Install from apt-get
apt-get update -yqq
apt-get install git -yqq
apt-get install wget -yqq
apt-get install zlib1g-dev -yqq
apt-get install libzip-dev -yqq
apt-get install unzip -yqq
apt-get install libpng-dev -yqq
apt-get install sqlite3 libsqlite3-dev -yqq
apt-get install libonig-dev -yqq
apt-get install libmagickwand-dev -yqq

wget https://getcomposer.org/download/2.0.13/composer.phar
mv ./composer.phar  /usr/local/bin/composer
chmod +x /usr/local/bin/composer
composer self-update

pecl install imagick-3.7.0
pecl install xdebug-3.1.0
# Here you can install any other extension that you need
docker-php-ext-enable xdebug
docker-php-ext-install pdo_sqlite
docker-php-ext-install mbstring
docker-php-ext-install zip
docker-php-ext-install gd
docker-php-ext-install intl
docker-php-ext-enable imagick

apt-get install -y locales >/dev/null
echo "de_CH UTF-8" > /etc/locale.gen
locale-gen de_CH.UTF-8
export LC_ALL=de_CH.UTF-8
